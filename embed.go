package main

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func embedMakeField(name string, value interface{}, inline bool) *discordgo.MessageEmbedField {
	return &discordgo.MessageEmbedField{
		Name:   name,
		Value:  fmt.Sprintf("%v", value),
		Inline: inline,
	}
}

package main

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

func fmtUsername(m *discordgo.Member) string {
	s := &strings.Builder{}

	s.WriteString(
		m.User.Username + "#" + m.User.Discriminator,
	)

	if m.Nick != "" {
		s.WriteByte(' ')

		s.WriteString(
			"(" + m.Nick + ")",
		)
	}

	return s.String()
}

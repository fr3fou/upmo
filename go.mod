module gitlab.com/diamondburned/upmo

go 1.12

require (
	github.com/DataDog/zstd v1.4.0 // indirect
	github.com/Necroforger/dgrouter v0.0.0-20190213194153-b723178091bc
	github.com/Necroforger/dgwidgets v0.0.0-20190131052008-56c8c1ca33e0
	github.com/Sereal/Sereal v0.0.0-20190529075751-4d99287c2c28 // indirect
	github.com/alfredxing/calc v0.0.0-20180827002445-77daf576f976
	github.com/asdine/storm v2.1.2+incompatible
	github.com/bwmarrin/discordgo v0.19.0
	github.com/davecgh/go-spew v1.1.1
	github.com/disintegration/imaging v1.6.0
	github.com/dustin/go-humanize v1.0.0
	github.com/golang/snappy v0.0.1 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	gitlab.com/diamondburned/block v0.0.0-20190408172153-54af7d5109b5
	gitlab.com/diamondburned/clocker v0.0.0-20190409150028-8b1f16fd9670
	gitlab.com/diamondburned/crawl-sticker v0.0.0-20190603012554-aef554ccc6f6
	gitlab.com/diamondburned/nixpkgs-db v0.0.0-20190513061749-a12323cadc09
	go.etcd.io/bbolt v1.3.2
	google.golang.org/appengine v1.6.0 // indirect
)

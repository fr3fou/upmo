package metrics

import (
	"log"
	"sync"
	"time"

	"github.com/asdine/storm"
)

// Metrics contains metrics data.
// The bot should only use the methods, thus all fields are private.
// The 2 exceptions for being public are Logf and Logfn, being the
// custom log functions.
type Metrics struct {
	sync.Mutex
	Logf  func(string, ...interface{})
	Logln func(...interface{})

	db   *storm.DB
	node storm.Node

	opts Options

	update chan struct{}
	stop   chan struct{}

	lastupdated *time.Time
	framedata   map[string]map[string]uint
}

// Options is used for metrics options.
// Default for Timeframe is 15 minutes.
type Options struct {
	Timeframe time.Duration
	TableName string
}

// User contains the structure for the table
type User struct {
	ID    string `storm:"unique,id"`
	Combo uint
	Score float64
}

// New initializes a new metrics client
func New(db *storm.DB, opts *Options) (*Metrics, error) {
	if opts == nil {
		opts = &Options{
			Timeframe: time.Duration(15 * time.Minute),
		}
	}

	m := &Metrics{
		Logf:      log.Printf,
		Logln:     log.Println,
		db:        db,
		node:      db.From("users"),
		opts:      *opts,
		update:    make(chan struct{}),
		stop:      make(chan struct{}),
		framedata: map[string]map[string]uint{},
	}

	go m.startTicker()

	return m, nil
}

// GetLastUpdated returns the last updated time. If
// this is nil, then the bot hasn't updated since startup.
func (m *Metrics) GetLastUpdated() *time.Time {
	return m.lastupdated
}

// Update manually pushes the frame regardless of the
// ticker
func (m *Metrics) Update() {
	m.update <- struct{}{}
}

// Stop frees up resources while stopping the main ticker
// loop. This will wait for events to finish and finally
// stop everything.
func (m *Metrics) Stop() {
	close(m.stop)
}

// GetUserDefault makes a default user
func (m *Metrics) GetUserDefault(uID string) *User {
	return &User{
		ID:    uID,
		Combo: 1,
		Score: 0.0,
	}
}

package metrics

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/asdine/storm"
	"github.com/asdine/storm/codec/protobuf"
	"github.com/bwmarrin/discordgo"
	"github.com/davecgh/go-spew/spew"
	bolt "go.etcd.io/bbolt"
)

func TestFull(t *testing.T) {
	phaser("Connect to the database file")

	os.Remove("/tmp/metrics-test.db")

	db, err := storm.Open(
		"/tmp/metrics-test.db",
		storm.BoltOptions(0640, &bolt.Options{Timeout: 1 * time.Second}),
		storm.Codec(protobuf.Codec),
	)

	if err != nil {
		t.Fatal(err)
	}

	defer db.Close()

	phaser("Initializes metrics")

	m, err := New(db, &Options{
		Timeframe: time.Second * 2,
	})

	if err != nil {
		t.Fatal(err)
	}

	defer m.Stop()

	ticker := time.NewTicker(time.Second)

	go func() {
		for {
			if _, ok := <-ticker.C; ok {
				fmt.Println("Sent an event to metrics")

				m.AddEventFromUser(&discordgo.User{
					ID:       "0",
					Username: "diamondburned",
				}, "unixporn")
			} else {
				break
			}
		}

		fmt.Println("Closed goroutine")
	}()

	time.Sleep(time.Second * 8)

	//ticker.Stop()
	//m.Stop()

	phaser("Query database")

	/*
		up := db.From("users").From("unixporn")
		up.All(&users)
	*/

	users := m.QueryUsers("unixporn")

	if len(users) < 1 {
		t.Fatal("Nothing in database")
	}

	spew.Dump(users)

	user := m.Query("unixporn", "0")
	if user == nil {
		t.Fatal("User not in database")
	}

	spew.Dump(user)
}

var i int

func phaser(s string) {
	i++
	fmt.Printf("Phase %d: %s\n", i, s)
}

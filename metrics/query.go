package metrics

import (
	"errors"
	"sort"
)

// ErrNilNode is returned when the node is nil
var ErrNilNode = errors.New("Node for guild is nil")

// Query queries the SQL database for a user
func (m *Metrics) Query(guildID, userID string) (*User, error) {
	gdb := m.node.From(guildID)

	if gdb == nil {
		return nil, ErrNilNode
	}

	var u User

	if err := gdb.One("ID", userID, &u); err != nil {
		return nil, err
	}

	return &u, nil
}

// QueryUsers queries the SQL database for top users
func (m *Metrics) QueryUsers(guildID string) ([]*User, error) {
	gdb := m.node.From(guildID)

	if gdb == nil {
		return nil, ErrNilNode
	}

	var users = []*User{}
	if err := gdb.All(&users); err != nil {
		return nil, err
	}

	sort.Slice(users, func(i, j int) bool {
		return users[i].Score > users[j].Score
	})

	return users, nil
}

package middlewares

import (
	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
)

// IsAdmin checks a message context and see if one's an
// administrator
func IsAdmin(ctx *exrouter.Context) bool {
	p, err := ctx.Ses.UserChannelPermissions(
		ctx.Msg.Author.ID,
		ctx.Msg.ChannelID,
	)

	if err == nil {
		if p&discordgo.PermissionAdministrator != 0 {
			return true
		}
	}

	return false
}

// AdminOnly is the middleware allowing only members with
// Administration powers
func AdminOnly(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		if ctx.Msg.GuildID == "" {
			return
		}

		if IsAdmin(ctx) {
			h(ctx)
		}
	}
}

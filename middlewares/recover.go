package middlewares

import (
	"fmt"

	"github.com/Necroforger/dgrouter/exrouter"
)

// Recover prevents a Goroutine panic
func Recover(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		defer func() {
			if r := recover(); r != nil {
				ctx.Reply(fmt.Sprintf("Recovered: %#v", r))
			}
		}()

		h(ctx)
	}
}

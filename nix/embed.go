package nix

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/nixpkgs-db"
)

// NixColor is the Nix embed color
const NixColor = 0x4A6CB0

// NixIconURL is the URL to the Nix icon, used in the embed
const NixIconURL = "https://nixos.org/logo/nixos-logo-only-hires.png"

// ConstructEmbed constructs an embed from packages
func ConstructEmbed(pkgs nixpkgs.Packages, commit string) []*discordgo.MessageEmbed {
	embeds := make([]*discordgo.MessageEmbed, 0, len(pkgs)/5+1)

	for i := 0; i < len(pkgs); i += 5 {
		embed := &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{
				Name:    fmt.Sprintf("Found %d packages", len(pkgs)),
				IconURL: NixIconURL,
			},
			Color:  NixColor,
			Fields: make([]*discordgo.MessageEmbedField, 0, 5),
			Footer: &discordgo.MessageEmbedFooter{
				Text: commit,
			},
		}

		for j := i; j < len(pkgs) && j < i+5; j++ {
			embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
				Name:  pkgs[j].NixID + " - " + pkgs[j].Name,
				Value: pkgs[j].Meta.Description,
			})
		}

		embeds = append(embeds, embed)
	}

	return embeds
}

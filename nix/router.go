package nix

import (
	"errors"
	"flag"
	"strings"
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/Necroforger/dgwidgets"
	"gitlab.com/diamondburned/nixpkgs-db"
)

const help = "Usage: nix [-desc __description__] [-name __name__] __nix name__"

// ErrNotEnoughArgs is returned when there aren't enough arguments
var ErrNotEnoughArgs = errors.New("Not enough arguments")

// Route is the plug-in router for Nix
func Route(rt *exrouter.Route) {
	rt.Cat("Nix")
	rt.Use(Check)

	rt.On("nix", search).
		Desc("Searches the online Nix packages repository")
}

func search(ctx *exrouter.Context) {
	if len(ctx.Args) < 2 {
		ctx.Reply(help)
		return
	}

	// Initialize (optional) strings
	var (
		descMatch string
		nameMatch string
	)

	// Parse the flags
	fs := flag.NewFlagSet("Nix", flag.ContinueOnError)
	fs.StringVar(&descMatch, "desc", "", "Matches package descriptions")
	fs.StringVar(&nameMatch, "name", "", "Matches package names")

	if err := fs.Parse(ctx.Args[1:]); err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	keyword := strings.Join(fs.Args(), " ")

	// Check if any queries are empty
	if keyword == "" && nameMatch == "" && descMatch == "" {
		ctx.Reply(help)
		return
	}

	// Fetch the packages
	pkgs, commit, err := nixpkgs.GetPackages()
	if err != nil {
		ctx.Reply("Error searching: ", err.Error())
		return
	}

	if len(pkgs) == 0 || commit == "" {
		ctx.Reply("Updating the Nixpkgs database...")
		return
	}

	pkgs = pkgs.Filter(func(p *nixpkgs.Package) bool {
		var s int

		// All 3 of these variables should not be empty, because
		// of the checks above

		if keyword == "" || strings.Contains(p.NixID, keyword) {
			s++
		}

		if nameMatch == "" || strings.Contains(p.Name, nameMatch) {
			s++
		}

		if descMatch == "" || strings.Contains(p.Meta.Description, descMatch) {
			s++
		}

		return s == 3
	})

	if len(pkgs) == 0 {
		ctx.Reply("No results found.")
		return
	}

	// Construct the embeds
	embeds := ConstructEmbed(pkgs, commit)

	// Create the paginator and add the embeds
	p := dgwidgets.NewPaginator(ctx.Ses, ctx.Msg.ChannelID)
	p.Loop = false
	p.DeleteReactionsWhenDone = true
	// p.ColourWhenDone = 0xFF0000
	p.Widget.Timeout = time.Second * 30
	p.Widget.UserWhitelist = []string{
		ctx.Msg.Author.ID,
	}

	p.Add(embeds...)
	p.SetPageFooters()
	p.Spawn()
}

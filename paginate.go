package main

import (
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/Necroforger/dgwidgets"
)

func newPaginator(ctx *exrouter.Context) *dgwidgets.Paginator {
	p := dgwidgets.NewPaginator(ctx.Ses, ctx.Msg.ChannelID)
	p.Loop = false
	p.DeleteReactionsWhenDone = true
	p.ColourWhenDone = 0xFF0000
	p.Widget.Timeout = time.Second * 30
	p.Widget.UserWhitelist = []string{
		ctx.Msg.Author.ID,
	}

	return p
}

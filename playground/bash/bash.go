package bash

import (
	"encoding/json"
	"net/url"

	"gitlab.com/diamondburned/upmo/playground"
	"gitlab.com/diamondburned/upmo/playground/client"
)

func init() {
	playground.AddPlayground("sh", exec)
	playground.AddPlayground("bash", exec)
	playground.AddPlayground("shell", exec)
}

func exec(header, body string) (*playground.Result, error) {
	var b = &bash{}
	var err error

	if body == "" {
		err = b.Execute(header)
	} else {
		err = b.ExecuteShorthand(header, body)
	}

	if err != nil {
		return nil, err
	}

	return &playground.Result{
		Output: b.GetOutput(),
		Errors: b.GetErrors(),
	}, nil
}

const (
	// Compile is the constant to the compile URL
	Compile = "https://rextester.com/rundotnet/Run"
)

// bash is the struct containing methods for playgrounds
type bash struct {
	Errors   string
	Files    string
	Result   string
	Stats    string
	Warnings string
}

// ExecuteShorthand executes the code with boilerplate code
func (b *bash) ExecuteShorthand(prefix, input string) error {
	return b.Execute(prefix + "\n" + input)
}

// Execute runs the code in the playground
func (b *bash) Execute(input string) error {
	req, err := client.Post(Compile, url.Values{
		"LanguageChoiceWrapper": {"38"},
		"EditorChoiceWrapper":   {"3"},
		"LayoutChoiceWrapper":   {"1"},
		"Program":               {input},
		"Input":                 {""},
		"Privacy":               {""},
		"PrivacyUsers":          {""},
		"Title":                 {""},
		"SavedOutput":           {""},
		"WholeError":            {""},
		"WholeWarning":          {""},
		"StatsToSave":           {""},
		"CodeGuid":              {""},
		"IsInEditMode":          {"False"},
		"IsLive":                {"False"},
	})

	if err != nil {
		return err
	}

	defer req.Body.Close()

	return json.NewDecoder(req.Body).Decode(&b)
}

// GetOutput returns the code output
func (b *bash) GetOutput() string {
	if b.Warnings != "" {
		return b.Warnings + "\n" + b.Result
	}

	return b.Result
}

// GetErrors returns the playgrond error
func (b *bash) GetErrors() string {
	if b.Errors != "" {
		return b.Errors + "\n" + b.Warnings
	}

	return ""
}

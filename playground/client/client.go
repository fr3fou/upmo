package client

import (
	"io"
	"net/http"
	"net/url"
	"time"
)

// Client sets a custom timeout for playgrounds.
// Playgrounds are expected to use this client for HTTP requests.
var Client = &http.Client{
	Timeout: 10 * time.Second,
}

// Post sends a POST request with the client
func Post(u string, body interface{}) (*http.Response, error) {
	switch body := body.(type) {
	case io.Reader:
		r, err := http.NewRequest("POST", u, body)
		if err != nil {
			return nil, err
		}

		r.Header.Set("Content-Type", "application/json")

		return Client.Do(r)
	case url.Values:
		return http.PostForm(u, body)
	default:
		r, err := http.NewRequest("POST", u, nil)
		if err != nil {
			return nil, err
		}

		return Client.Do(r)
	}
}

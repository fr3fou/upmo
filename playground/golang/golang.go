package golang

import (
	"encoding/json"
	"net/url"
	"strings"

	"gitlab.com/diamondburned/upmo/playground"
	"gitlab.com/diamondburned/upmo/playground/client"
)

func init() {
	playground.AddPlayground("go", exec)
	playground.AddPlayground("golang", exec)
}

func exec(header, body string) (*playground.Result, error) {
	var g = &golang{}
	var err error

	if body == "" {
		err = g.Execute(header)
	} else {
		err = g.ExecuteShorthand(header, body)
	}

	if err != nil {
		return nil, err
	}

	return &playground.Result{
		Output: g.GetOutput(),
		Errors: g.GetErrors(),
	}, nil
}

const (
	// Compile is the constant to the compile URL
	Compile = "https://play.golang.org/compile"

	// Fmt is the constant to the gofmt/goimports URL
	Fmt = "https://play.golang.org/fmt"
)

// golang is the struct containing methods for playgrounds
type golang struct {
	Errors string `json:"Errors"`
	Events []struct {
		Message string `json:"Message"`
		Kind    string `json:"Kind"`
		Delay   int64  `json:"Delay"`
	} `json:"Events"`

	Status      int64 `json:"Status"`
	IsTest      bool  `json:"IsTest"`
	TestsFailed int64 `json:"TestsFailed"`

	imports struct {
		Body  string
		Error string
	}
}

// ExecuteShorthand executes the code with boilerplate code
func (g *golang) ExecuteShorthand(prefix, input string) error {
	return g.Execute(`package main

` + prefix + `

func main() {
    ` + input + `
}`)
}

// Execute runs the code in the playground
func (g *golang) Execute(input string) error {
	if err := g.goimports(input); err != nil {
		return err
	}

	// Imports errored out
	if g.imports.Error != "" {
		return nil
	}

	req, err := client.Post(Compile, url.Values{
		"version": {"2"},
		"body":    {g.imports.Body},
	})

	if err != nil {
		return err
	}

	defer req.Body.Close()

	return json.NewDecoder(req.Body).Decode(&g)
}

func (g *golang) goimports(input string) error {
	req, err := client.Post(Fmt, url.Values{
		"body":    {input},
		"imports": {"true"},
	})

	if err != nil {
		return err
	}

	defer req.Body.Close()

	return json.NewDecoder(req.Body).Decode(&g.imports)
}

// GetOutput returns the code output
func (g *golang) GetOutput() string {
	var s strings.Builder
	for _, e := range g.Events {
		s.WriteString(e.Message)
	}

	return s.String()
}

// GetErrors returns the playgrond error
func (g *golang) GetErrors() string {
	if g.imports.Error != "" {
		s := &strings.Builder{}

		s.WriteString("Error formatting:\n")
		s.WriteString(g.imports.Error)

		s.WriteString(g.Errors)

		return s.String()
	}

	return g.Errors
}

package golang

import (
	"testing"
)

func TestRust(t *testing.T) {
	code := `fmt.Println("Renzix gay")`
	g := &golang{}
	if err := g.ExecuteShorthand("", code); err != nil {
		t.Fatal(err)
	}

	println(g.GetOutput())

	if err := g.ExecuteShorthand("", code[7:] /*Emulate error*/); err != nil {
		t.Fatal(err)
	}

	println("Output: \n" + g.GetOutput())
	println("Error: \n" + g.GetErrors())
}

package playground

import "strings"

// Language is the type for all languages
type Language string

// Result is the result type for playgrounds
type Result struct {
	Output string
	Errors string
	Image  []byte
}

// Playground is the function signature for executing playground code. This
// function should not execute a shorthand code if body == "", and instead
// treat header as the whole code.
// The return values should return the results and the error, respectively.
type Playground func(header, body string) (*Result, error)

// playgrounds contains all language playgrounds
var playgrounds = map[Language]Playground{}

// AddPlayground adds a language into the list. This is used for dash imports.
func AddPlayground(lang Language, p Playground) {
	playgrounds[lang] = p
}

// ReflectLanguage reflects the language string for a Playground
func ReflectLanguage(l Language) Playground {
	if p, ok := playgrounds[l]; ok {
		return p
	}

	return nil
}

// Execute executes the code in the playground.
func (p Playground) Execute(code string, shorthand bool) (*Result, error) {
	if shorthand {
		lines := strings.Split(code, "\n")
		body := make([]string, 0, len(lines))
		head := make([]string, 0, len(lines))

		for _, l := range lines {
			l = strings.TrimSpace(l)

			if len(l) > 0 && l[0] == ':' {
				head = append(head, l[1:])
			} else {
				body = append(body, l)
			}
		}

		return p(
			strings.Join(head, "\n"),
			strings.Join(body, "\n"),
		)
	}

	return p(code, "")
}

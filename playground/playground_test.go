package playground

import (
	"fmt"
	"testing"

	_ "gitlab.com/diamondburned/upmo/playground/golang"
)

func TestExecute(t *testing.T) {
	p := ReflectLanguage("go")
	if p == nil {
		t.Fatal("Playground did not match Go")
	}

	s, err := p.Execute(`:import "fmt"
 :import "time"
fmt.Println(time.Now().Format(time.RFC3339))`, true)

	if err != "" {
		t.Fatal(err)
	}

	fmt.Println(s)
}

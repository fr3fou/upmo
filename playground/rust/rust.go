package rust

import (
	"bytes"
	"encoding/json"

	"gitlab.com/diamondburned/upmo/playground"
	"gitlab.com/diamondburned/upmo/playground/client"
)

func init() {
	playground.AddPlayground("rs", exec)
	playground.AddPlayground("rust", exec)
}

func exec(header, body string) (*playground.Result, error) {
	var rs = &rust{}
	var err error

	if body == "" {
		err = rs.Execute(header)
	} else {
		err = rs.ExecuteShorthand(header, body)
	}

	if err != nil {
		return &playground.Result{
			Errors: err.Error(),
		}, nil
	}

	return &playground.Result{
		Output: rs.GetOutput(),
		Errors: rs.GetErrors(),
	}, nil
}

const (
	// Compile is the constant to the compile URL
	Compile = "https://play.rust-lang.org/execute"

	rustChannel = "stable"
	rustEdition = "2018"
	rustMode    = "debug"
)

type rustDefaults struct {
	Channel   string `json:"channel"`
	Mode      string `json:"mode"`
	Edition   string `json:"edition"`
	CrateType string `json:"crateType"`
	Tests     bool   `json:"tests"`
	Code      string `json:"code"`
	Backtrace bool   `json:"backtrace"`
}

// rust is the struct containing methods for playgrounds
type rust struct {
	Success bool   `json:"success"`
	Stdout  string `json:"stdout"`
	Stderr  string `json:"stderr"`
}

// ExecuteShorthand executes the code with boilerplate code
func (r *rust) ExecuteShorthand(prefix, input string) error {
	return r.Execute(prefix + `

fn main() {
    ` + input + `
}`)
}

// Execute runs the code in the playground
func (r *rust) Execute(input string) error {
	s, err := json.Marshal(&rustDefaults{
		Channel:   rustChannel,
		Mode:      rustMode,
		Edition:   rustEdition,
		CrateType: "bin",
		Tests:     false,
		Code:      input,
		Backtrace: false,
	})

	if err != nil {
		return err
	}

	rq, err := client.Post(Compile, bytes.NewReader(s))
	if err != nil {
		return err
	}

	defer rq.Body.Close()

	/*
			2019/04/11 12:37:40 {"error":"No request was provided"}

		b, _ := ioutil.ReadAll(rq.Body)
		log.Println(string(b))
		return nil
	*/

	return json.NewDecoder(rq.Body).Decode(&r)
}

// GetOutput returns the code output
func (r *rust) GetOutput() string {
	return r.Stderr + "\n" + r.Stdout
}

// GetErrors returns the playgrond error
func (r *rust) GetErrors() string {
	if !r.Success {
		return r.Stderr
	}

	return ""
}

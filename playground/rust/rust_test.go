package rust

import (
	"strings"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestRust(t *testing.T) {
	code := `println!("Renzix gay")`
	r := &rust{}
	if err := r.ExecuteShorthand("", code); err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(r.GetOutput(), "Renzix gay") {
		t.Fatal("Got wrong output: " + r.GetOutput())
	}

	if err := r.ExecuteShorthand("", code[1:] /*Emulate error*/); err != nil {
		t.Fatal(err)
	}

	spew.Dump(r)
}

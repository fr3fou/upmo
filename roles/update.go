package roles

import (
	"sort"

	"github.com/bwmarrin/discordgo"
)

// Update cycles through all regions and update their children
// IDs. Run this after you've made changes to the roles.
func Update(d *discordgo.Session, guildID string) error {
	gdb, err := node.From(guildID).Begin(true)
	if err != nil {
		return err
	}

	defer gdb.Rollback()

	var regions []*Region
	if err := gdb.All(&regions); err != nil {
		return err
	}

	for _, rg := range regions {
		r, err := d.State.Role(guildID, rg.StartID)
		if err != nil {
			return err
		}

		rg.ptr = r
	}

	sort.Slice(regions, func(i, j int) bool {
		return regions[i].ptr.Position > regions[j].ptr.Position
	})

	roles, err := d.GuildRoles(guildID)
	if err != nil {
		return err
	}

	// Sort role positions
	sort.Slice(roles, func(i, j int) bool {
		return roles[i].Position > roles[j].Position
	})

	var (
		j     int
		sweep bool
	)

	for i := 0; i < len(regions); i++ {
		r := regions[i]
		r.ChildrenIDs = []string{}

		//  ignore @everyone v
		for ; j < len(roles)-1; j++ {
			// This should only be true once ever.
			if r.StartID == roles[j].ID {
				sweep = true
				continue
			}

			// This checks the next roles as we sweep
			if i+1 < len(regions) {
				if regions[i+1].StartID == roles[j].ID {
					break
				}
			}

			if sweep {
				r.ChildrenIDs = append(r.ChildrenIDs, roles[j].ID)
			}
		}

		if err := gdb.Save(r); err != nil {
			return err
		}
	}

	return gdb.Commit()
}

package main

import (
	"fmt"
	"io"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/Necroforger/dgrouter/exmiddleware"
	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/upmo/middlewares"
	"gitlab.com/diamondburned/upmo/nix"
	"gitlab.com/diamondburned/upmo/tags"
)

// NewRouter makes a new dgo router
func NewRouter() (r *exrouter.Route) {
	r = exrouter.New()
	r.Use(middlewares.Recover)
	r.Use(middlewares.Typing)
	r.Use(middlewares.GuildOnly)
	r.Use(exmiddleware.UserCooldown(
		time.Second*3,
		exmiddleware.CatchReply("Slow down!"),
	))

	r.Group(nix.Route)
	r.Group(tags.Route)
	r.Group(routerStickers)

	r.Group(func(rt *exrouter.Route) {
		rt.Cat("Utilties")

		rt.On("calc", routerCalc).Desc("Evaluates an expression and return its result")
	})

	r.Group(func(rt *exrouter.Route) {
		rt.Cat("Fun")

		rt.On("choose", routerChoose).Desc("Chooses a random choice: \"a or b or ...\"")
		rt.On("playground", routerPlayground).Desc("Runs Go/Rust code in a playground")
	})

	r.Group(func(rt *exrouter.Route) {
		rt.Cat("Roles")

		rt.On("role", routerRolesAssign).Desc("Assign or remove your role")
		rt.On("list", routerRolesList).Desc("Shows all or a single assignable role(s)")
	})

	r.Group(func(rt *exrouter.Route) {
		rt.Cat("Ranking")

		rt.On("xp", routerXP).Desc("Shows all top members")
		rt.On("me", routerMe).Desc("Shows my information")
	})

	r.Group(func(rt *exrouter.Route) {
		rt.Cat("Administration")
		rt.Use(middlewares.AdminOnly)

		rt.On("sb", routerStarboard).Desc("(Re)sets the Starboard settings")
		rt.On("stalin", routerStalin).Desc("Stalin settings")
		rt.On("reset", routerResetXP).Desc("Reset someone's metrics data")
		rt.On("dropmet", routerDropMet).Desc("Drop all metrics for this guild")
		rt.On("clean", routerClean).Desc("Deletes all the bot's messages, defaulted to in the past 100")
		rt.On("log", routerLogger).Desc("Sets a channel to log all errors")
		rt.On("roles", routerRoles).Desc("Role options for this guild")

		rt.On("say", func(ctx *exrouter.Context) {
			ctx.Ses.ChannelMessageDelete(ctx.Msg.ChannelID, ctx.Msg.ID)
			content := strings.Join(strings.Fields(ctx.Msg.Content)[1:], " ")
			sendToDiscord(ctx, content)
		})

		rt.On("die", func(ctx *exrouter.Context) {
			stop <- struct{}{}
		})

		rt.On("gc", routerGC).Desc("Runs the garbage collector")
	})

	r.Default = r.On("help", func(ctx *exrouter.Context) {
		s := strings.Builder{}
		w := defaultTabWriter(&s)

		l := ""
		a := middlewares.IsAdmin(ctx)

		for _, v := range r.Routes {
			if v.Category == "Administration" && !a {
				continue
			}

			if l != v.Category {
				fmt.Fprintf(w,
					"\n%s\n",
					strings.ToUpper(v.Category),
				)

				l = v.Category
			}

			fmt.Fprintf(w,
				"   %s\t%s\n",
				v.Name, v.Description,
			)
		}

		w.Flush()

		sendToDiscord(ctx, "```"+s.String()+"```")
	}).Desc("Prints this help menu")

	return r
}

func defaultTabWriter(w io.Writer) *tabwriter.Writer {
	return (&tabwriter.Writer{}).Init(w, 0, 4, 2, ' ', 0)
}

package main

import (
	"fmt"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/alfredxing/calc/compute"
)

func routerCalc(ctx *exrouter.Context) {
	r, err := compute.Evaluate(ctx.Args.After(1))
	if err != nil {
		sendToDiscord(ctx, fmt.Sprintf("Error: ```%v```", err))
	}

	sendToDiscord(ctx, fmt.Sprintf("```%f```", r))
}

package main

import (
	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/upmo/choose"
	"gitlab.com/diamondburned/upmo/utils"
)

func routerChoose(ctx *exrouter.Context) {
	if ctx.Msg.MentionEveryone {
		sendToDiscord(ctx, "Nope.")
		return
	}

	c, err := choose.Eval(ctx.Args.After(1))
	if err != nil {
		sendToDiscord(ctx, err.Error())
		return
	}

	sendToDiscord(ctx, utils.Sanitize(c))
}

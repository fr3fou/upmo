package main

import (
	"strconv"

	"github.com/Necroforger/dgrouter/exrouter"
)

func routerClean(ctx *exrouter.Context) {
	var fetch = 100
	if a := ctx.Args.Get(1); a != "" {
		fetch, _ = strconv.Atoi(a)
		if fetch <= 0 || fetch > 100 {
			sendToDiscord(ctx, "Invalid range, must satisfy 0 < f <= 100")
			return
		}
	}

	m, err := d.ChannelMessages(ctx.Msg.ChannelID, 100, "", "", "")
	if err != nil {
		errToDiscord(ctx, err)
		return
	}

	var IDslice = make([]string, 0, len(m))
	for _, m := range m {
		if m.Author.ID == d.State.User.ID {
			IDslice = append(IDslice, m.ID)
		}
	}

	IDslice = append(IDslice, ctx.Msg.ID)

	if err := d.ChannelMessagesBulkDelete(ctx.Msg.ChannelID, IDslice); err != nil {
		errToDiscord(ctx, err)
		return
	}

	sendToDiscord(ctx, "Done.")
}

package main

import (
	"bytes"
	"regexp"
	"strings"
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/playground"
	"gitlab.com/diamondburned/upmo/utils"

	_ "gitlab.com/diamondburned/upmo/playground/bash"
	_ "gitlab.com/diamondburned/upmo/playground/golang"
	_ "gitlab.com/diamondburned/upmo/playground/latex"
	_ "gitlab.com/diamondburned/upmo/playground/rust"
)

var markdownLanguageRegex = regexp.MustCompile("^```(\\S+)\\n")

func routerPlayground(ctx *exrouter.Context) {
	var lang, code string

	switch firstArg := ctx.Args.Get(1); {
	case firstArg == "":
		sendToDiscord(ctx, "Usage: \n```\nplayground `\u200b``[lang]\n:prefix"+
			" statement\ncode\n`\u200b`\u200b`\n```")

		return
	case strings.HasPrefix(firstArg, "```"):
		code = getRestOfSlice(ctx, 1)
		m := markdownLanguageRegex.FindAllStringSubmatch(code, -1)

		if m != nil && len(m) > 0 {
			lang = m[0][1]
			code = strings.TrimPrefix(code, m[0][0])
		}

		code = strings.Trim(code, "`")
	default:
		lang = firstArg
		code = strings.Trim(getRestOfSlice(ctx, 2), "`")
	}

	p := playground.ReflectLanguage(playground.Language(lang))
	if p == nil {
		sendToDiscord(ctx, "No matching languages.")
		return
	}

	m, err := ctx.Member(ctx.Msg.GuildID, ctx.Msg.Author.ID)
	if err != nil {
		errToDiscord(ctx, err)
		return
	}

	var displayname = m.User.String()
	if m.Nick != "" {
		displayname = m.Nick
	}

	s := &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{
				Name:    displayname,
				IconURL: m.User.AvatarURL("32"),
			},
		},
	}
	now := time.Now()

	result, err := p.Execute(code, true)

	if err != nil {
		sendToDiscord(ctx, "Error: "+err.Error())
		return
	}

	if result.Errors != "" {
		s.Embed.Color = 0xFF0000
		s.Embed.Description = "```\n" + utils.EscapeCodeBlock(result.Errors) + "\n```"
	} else {
		if result.Image != nil {
			s.Files = []*discordgo.File{
				&discordgo.File{
					Name:        "image.png",
					ContentType: "image/png",
					Reader:      bytes.NewReader(result.Image),
				},
			}

			s.Embed.Image = &discordgo.MessageEmbedImage{
				URL: "attachment://image.png",
			}
		} else if result.Output == "" || result.Output == "\n" {
			result.Output = "<empty>"
		}

		if result.Output != "" {
			s.Embed.Description = "```\n" + utils.EscapeCodeBlock(result.Output) + "\n```"
		}

		s.Embed.Color = 0x00FF00
	}

	dura := time.Now().Sub(now)
	s.Embed.Footer = &discordgo.MessageEmbedFooter{
		Text: dura.String(),
	}

	s.Embed.Timestamp = now.Format(time.RFC3339)

	sendToDiscord(ctx, s)
}

func getRestOfSlice(ctx *exrouter.Context, i int) string {
	fields := strings.Split(ctx.Msg.Content, " ")
	if i > len(fields) {
		return ""
	}

	return strings.Join(fields[i:], " ")
}

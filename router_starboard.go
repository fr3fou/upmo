package main

import (
	"regexp"
	"strconv"

	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/upmo/starboard"
)

var patternChannels = regexp.MustCompile("<#[^>]*>")

func routerStarboard(ctx *exrouter.Context) {
	switch ctx.Args.Get(1) {
	case "setup":
		a := ctx.Args.Get(2)
		if a == "" {
			sendToDiscord(ctx, "Missing target channel")
			return
		}

		if err := starboard.Setup(ctx.Ses, ctx.Msg.GuildID, a[2:len(a)-1]); err != nil {
			errToDiscord(ctx, err)
			return
		}

		sendToDiscord(ctx, "Done.")
	case "star":
		if err := starboard.ChangeRxn(ctx.Ses, ctx.Msg.GuildID, ctx.Args.Get(2)); err != nil {
			errToDiscord(ctx, err)
			return
		}

		sendToDiscord(ctx, "Done.")
	case "count":
		a := ctx.Args.Get(2)
		i, err := strconv.Atoi(a)
		if err != nil {
			errToDiscord(ctx, err)
			return
		}

		if !(0 < i) {
			sendToDiscord(ctx, "Count doesn't satisfy `0 < i`")
			return
		}

		if err := starboard.ChangeStarcount(ctx.Ses, ctx.Msg.GuildID, uint(i)); err != nil {
			errToDiscord(ctx, err)
			return
		}

		sendToDiscord(ctx, "Done.")
	case "blacklist":
		a := ctx.Args.Get(2)
		if a == "" {
			sendToDiscord(ctx, "Missing target channel")
			return
		}

		b, err := starboard.ToggleBlacklist(ctx.Ses, ctx.Msg.GuildID, a[2:len(a)-1])
		if err != nil {
			errToDiscord(ctx, err)
			return
		}

		if b {
			sendToDiscord(ctx, "Added channel to blacklist.")
		} else {
			sendToDiscord(ctx, "Removed channel from blacklist.")
		}
	case "reset":
		if err := starboard.ResetMessage(ctx.Ses, ctx.Msg.ChannelID, ctx.Args.Get(2)); err != nil {
			errToDiscord(ctx, err)
			return
		}

		sendToDiscord(ctx, "Done.")
	default:
		sendToDiscord(ctx, `Missing command:
	- setup
	- star [emoji]
	- count [int; 0 < i]
	- blacklist [channel]
	- reset [messageID]`)
	}
}

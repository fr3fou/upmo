package smartlink

import "regexp"

// (empty) so it matches standard links
// | OR
// canary. matches canary URL
// 3 `(\d+)` for guild ID, channel ID and message ID
var smartLinkChannelURL = regexp.MustCompile(`https://(|canary.)discordapp.com/channels/(\d+)/(\d+)/(\d+)`)

package stalin

import (
	"fmt"
	"strings"
	"unicode"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

// HandlerCreate wraps around MessageCreate events
func HandlerCreate(d *discordgo.Session, c *discordgo.MessageCreate) {
	Handler(d, c.Message)
}

// HandlerEdit wraps around MessageEdit events
func HandlerEdit(d *discordgo.Session, e *discordgo.MessageUpdate) {
	defer func() {
		if r := recover(); r != nil {
			logger.Error(d, e.GuildID, fmt.Errorf("Panic: %#v", r))
		}
	}()

	// Update messages do not have author, working around that
	m, err := d.State.Message(e.ChannelID, e.ID)
	if err != nil {
		m, err = d.ChannelMessage(e.ChannelID, e.ID)
		if err != nil {
			return
		}
	}

	// Workaround for MessageUpdate not having the Author struct
	// filled.
	e.Author = m.Author

	Handler(d, e.Message)
}

// Handler handles all incoming messages and delete them if
// blacklisted words are found
func Handler(d *discordgo.Session, m *discordgo.Message) {
	IDs := CheckDelete(m)
	var err error

	switch {
	case IDs == nil, len(IDs) == 0:
		return
	case len(IDs) == 1:
		err = d.ChannelMessageDelete(m.ChannelID, IDs[0])
	case len(IDs) > 1:
		err = d.ChannelMessagesBulkDelete(m.ChannelID, IDs)
	}

	if err != nil {
		logger.Error(d, m.GuildID, err)
	}
}

// CheckDelete checks one or multiple messages for blacklisted words.
// Nothing is returned if the message does not have the word.
func CheckDelete(m *discordgo.Message) []string {
	if db == nil {
		return nil
	}

	var s Settings

	// Check if the guild is in the database
	if err := node.One("GuildID", m.GuildID, &s); err != nil {
		// Drop if it's not, it's not our business
		return nil
	}

	// Check if the channel is in one of the whitelists
	for _, g := range s.ExceptionChannels {
		if g == m.ChannelID {
			return nil
		}
	}

	// Trim ALL whitespace characters
	content := strings.Map(func(r rune) rune {
		if unicode.IsLetter(r) {
			return unicode.ToLower(r)
		}

		return -1
	}, m.Content)

	// See if the message contains any blacklisted words
	for _, w := range s.BlacklistedWords {
		if strings.Contains(content, w) {
			return []string{m.ID}
		}
	}

	return checkInStore(m, s.BlacklistedWords)
}

package stalin

import "strings"

func wordInSlice(sl []*message, word string) (ids []string) {
	var s string

	for i := len(sl) - 1; i >= 0; i-- {
		// Prepend the last message into the buffer
		s = sl[i].Content + s

		// Check for blacklisted word
		if strings.HasPrefix(s, word) {
			ms := sl[i:]
			ids = make([]string, len(ms))

			for i, m := range ms {
				ids[i] = m.ID
			}

			return
		}
	}

	return nil
}

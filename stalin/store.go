package stalin

import (
	"sync"

	"github.com/bwmarrin/discordgo"
)

type message struct {
	ID      string
	Content string
}

var store = &struct {
	sync.Mutex
	Map map[string]map[string][]*message
}{
	Map: map[string]map[string][]*message{},
}

func checkInStore(m *discordgo.Message, words []string) []string {
	if m.Author == nil {
		return nil
	}

	store.Lock()
	defer store.Unlock()

	if _, ok := store.Map[m.GuildID]; !ok {
		store.Map[m.GuildID] = make(map[string][]*message)
	}

	s := store.Map[m.GuildID][m.Author.ID]
	s = append(s, &message{
		m.ID, m.Content,
	})

	if len(s) > 25 {
		s = s[25:]
	}

	var ids []string

	for _, w := range words {
		ids = wordInSlice(s, w)
		if ids != nil && len(ids) > 0 {
			s = s[:len(s)-len(ids)]
			break
		}
	}

	store.Map[m.GuildID][m.Author.ID] = s

	return ids
}

// Package starboard provides a plug-in handler for Discord starboard, using storm as its data store
package starboard

import (
	"errors"

	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

var (
	errUnitialized = errors.New("Fatal error: starboard unintialized with Initialize()")
)

// Settings contains settings for a guild's starboard
type Settings struct {
	GuildID string `storm:"unique,id"`

	WebhookID      string `storm:"unique"`
	WebhookToken   string `storm:"unique"`
	WebhookChannel string `stomr:"unique"`

	Starcount uint
	Reaction  string

	// Blacklisted channels
	Blacklist []string
}

// Message is a type for a starred message
type Message struct {
	ID        string `storm:"unique"`
	ChannelID string
	GuildID   string
}

// Setup sets up a starboard for a guild
func Setup(d *discordgo.Session, guildID, channelID string) error {
	g, err := d.State.Guild(guildID)
	if err != nil {
		return err
	}

	c, err := d.State.Channel(channelID)
	if err != nil {
		return err
	}

	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	var s Settings
	if err := node.One("GuildID", guildID, &s); err == nil {
		// Drop all past messages
		node.Drop(s.WebhookChannel)

		// Delete the old webhook
		d.WebhookDeleteWithToken(s.WebhookID, s.WebhookToken)
	}

	var webhookName = "Starboard hook: " + c.Name
	if len(webhookName) > 32 {
		webhookName = webhookName[:29] + "..."
	}

	w, err := d.WebhookCreate(c.ID, webhookName, "")
	if err != nil {
		return err
	}

	// Initializes the store for that guild
	node.From(c.ID).Init(&([]*Message{}))

	s = Settings{
		GuildID:        g.ID,
		WebhookID:      w.ID,
		WebhookToken:   w.Token,
		WebhookChannel: c.ID,
		Starcount:      4,
		Reaction:       "\u2b50", // ⭐
	}

	if err := node.Save(&s); err != nil {
		if err != storm.ErrAlreadyExists {
			return err
		}
	}

	return node.Commit()
}

// ChangeRxn changes the rune for the reaction
func ChangeRxn(d *discordgo.Session, guildID string, star string) error {
	return updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		s.Reaction = star
		return node.Save(&s)
	})
}

// ChangeStarcount updates the starcount for a guild
func ChangeStarcount(d *discordgo.Session, guildID string, count uint) error {
	return updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		s.Starcount = count
		return node.Save(&s)
	})
}

// ToggleBlacklist adds or removes a channel from the blacklist. True means
// added and false means removed.
func ToggleBlacklist(d *discordgo.Session, guildID, channelID string) (b bool, e error) {
	return b, updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		for i, c := range s.Blacklist {
			if c == channelID {
				s.Blacklist = append(
					s.Blacklist[:i],
					s.Blacklist[i+1:]...,
				)

				b = false
				return node.Save(&s)
			}
		}

		b = true
		s.Blacklist = append(s.Blacklist, channelID)

		return node.Save(&s)
	})
}

// ResetMessage deletes the message from the store in case it bugged out
func ResetMessage(d *discordgo.Session, channelID, messageID string) error {
	c, err := d.State.Channel(channelID)
	if err != nil {
		return err
	}

	if c.GuildID == "" {
		return errors.New("Not in a guild")
	}

	var s Settings
	if err := node.One("GuildID", c.GuildID, &s); err != nil {
		return err
	}

	gdb, err := node.From(s.WebhookChannel).Begin(true)
	if err != nil {
		return err
	}

	defer gdb.Rollback()

	if err := gdb.DeleteStruct(&Message{
		ID: messageID,
	}); err != nil {
		return err
	}

	return gdb.Commit()
}

func updateNode(f func(node storm.Node) error) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}

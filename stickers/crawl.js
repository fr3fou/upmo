i = document.createElement("textarea")
document.body.appendChild(i)

document.querySelectorAll(".sticker").forEach(d => {
    i.value += `${d.attributes["data-clipboard-text"].value.slice(1, -1)}|${d.querySelector("img").src}`
	i.value += "\n"
})

i.select()
document.execCommand("copy")


package tags

import (
	"fmt"

	"github.com/Necroforger/dgrouter/exrouter"
)

func del(ctx *exrouter.Context) {
	g, err := ctx.Guild(ctx.Msg.GuildID)
	if err != nil {
		ctx.Reply("Error fetching guild: " + err.Error())
		return
	}

	t, err := GetTag(g, ctx.Args.Get(1))
	if err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	b, err := tagUserAllowed(ctx, t)
	if err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	if !b {
		ctx.Reply("Tag is not yours.")
		return
	}

	if err := RemoveTag(g, t.Name); err != nil {
		ctx.Reply("Error: " + err.Error())
	}

	ctx.Reply(fmt.Sprintf(
		"Removed tag %s of user <@%s>.",
		t.Name, t.Author,
	))
}

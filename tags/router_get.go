package tags

import (
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/upmo/logger"
	"gitlab.com/diamondburned/upmo/utils"
)

func get(ctx *exrouter.Context) {
	g, err := ctx.Guild(ctx.Msg.GuildID)
	if err != nil {
		ctx.Reply("Error fetching guild: " + err.Error())
		return
	}

	t, err := GetTag(g, ctx.Args.After(1))
	if err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	s := strings.Builder{}
	s.WriteString(utils.Sanitize(t.Content))

	if len(t.Attachments) > 0 {
		for i := 0; i < len(t.Attachments); i++ {
			s.WriteByte('\n')
			s.WriteString(t.Attachments[i])
		}
	}

	_, err = ctx.Ses.ChannelMessageSend(ctx.Msg.ChannelID, s.String())
	if err != nil {
		logger.Error(ctx.Ses, ctx.Msg.GuildID, err)
	}
}

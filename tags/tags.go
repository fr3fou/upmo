package tags

import (
	"strings"
	"time"

	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

// Tag contains a tag. The methods provided follow the
// CRUD interface.
type Tag struct {
	Name        string `storm:"id"`
	Content     string
	Attachments []string // List of URLs

	// Author ID
	Author    string
	Timestamp time.Time
}

// AddTag adds a tag for a guild.
func AddTag(g *discordgo.Guild, t *Tag) error {
	return tx(g.ID, func(n storm.Node) error {
		return n.Save(t)
	})
}

// GetTag gets a tag for a guild.
func GetTag(g *discordgo.Guild, name string) (*Tag, error) {
	var t Tag

	if err := tx(g.ID, func(n storm.Node) error {
		return n.One("Name", name, &t)
	}); err != nil {
		return nil, err
	}

	return &t, nil
}

// RemoveTag removes a tag for a guild.
func RemoveTag(g *discordgo.Guild, name string) error {
	return tx(g.ID, func(n storm.Node) error {
		var t Tag
		if err := n.One("Name", name, &t); err != nil {
			return err
		}

		return n.DeleteStruct(&t)
	})
}

// SearchTags queries all tags if keyword is empty, else it filters out. The
// filter is case-insensitive.
func SearchTags(g *discordgo.Guild, keyword string) ([]*Tag, error) {
	var tags = []*Tag{}

	if err := tx(g.ID, func(n storm.Node) error {
		return n.All(&tags)
	}); err != nil {
		return nil, err
	}

	if keyword != "" {
		var filtered = make([]*Tag, 0, 10) // not pre-alloc to prevent OOM
		var keyword = strings.ToLower(keyword)

		for _, t := range tags {
			if strings.Contains(strings.ToLower(t.Name), keyword) {
				filtered = append(filtered, t)
			}
		}

		return filtered, nil
	}

	return tags, nil
}

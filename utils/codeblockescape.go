package utils

import "strings"

var codeblockEscaper = strings.NewReplacer(
	"```", "`\u200b`\u200b`\u200b",
)

// EscapeCodeBlock escapes the codeblocks to sanittize output.
func EscapeCodeBlock(cb string) string {
	return codeblockEscaper.Replace(cb)
}

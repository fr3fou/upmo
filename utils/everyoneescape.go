package utils

import "strings"

var everyoneEscaper = strings.NewReplacer(
	"@everyone", "@\u200be\u200bver\u200byo\u200bne",
	"@here", "@\u200bh\u200ber\u200be",
)

// EscapeEveryone escapes @everyone and @here mentions.
func EscapeEveryone(s string) string {
	return everyoneEscaper.Replace(s)
}

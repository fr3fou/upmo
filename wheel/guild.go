package wheel

import (
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

// Guild contains all guild settings
type Guild struct {
	GuildID    string `storm:"unique,id"`
	LogChannel string `storm:"unique"`

	Threshold int

	AdminRoles []string
	Wheelies   []string
}

// Setup sets up the guild's settings and initalizes the database. This will also reset past settings, along with all the votes.
func Setup(d *discordgo.Session, guildID, channelID string) error {
	g, err := d.State.Guild(guildID)
	if err != nil {
		return err
	}

	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	node.Drop(g.ID)
	node.From(g.ID).Init(&([]*Vote{}))

	node.Save(&Guild{
		GuildID:    g.ID,
		LogChannel: channelID,
	})

	return node.Commit()
}

// ChangeLogChannel changes the log channel for the votes.
func ChangeLogChannel(d *discordgo.Session, guildID, channelID string) error {
	return updateNode(func(node storm.Node) error {
		var g Guild
		if err := node.One("GuildID", guildID, &g); err != nil {
			return err
		}

		g.LogChannel = channelID

		return node.Save(&g)
	})
}

// ChangeThreshold changes the vote threshold for the guild.
func ChangeThreshold(d *discordgo.Session, guildID string, threshold int) error {
	return updateNode(func(node storm.Node) error {
		var g Guild
		if err := node.One("GuildID", guildID, &g); err != nil {
			return err
		}

		g.Threshold = threshold

		return node.Save(&g)
	})
}

// ToggleAdminRole adds or removes an admin role from the store.
func ToggleAdminRole(d *discordgo.Session, guildID string, r *discordgo.Role) error {
	return updateNode(func(node storm.Node) error {
		var g Guild
		if err := node.One("GuildID", guildID, &g); err != nil {
			return err
		}

		for i, id := range g.AdminRoles {
			if id == r.ID {
				g.AdminRoles = deleteString(g.AdminRoles, i)
				goto Save
			}
		}

		g.AdminRoles = append(g.AdminRoles, r.ID)

	Save:
		return node.Save(&g)
	})
}

// ToggleWheelies adds or removes a wheelie role from the store.
func ToggleWheelies(d *discordgo.Session, guildID string, r *discordgo.Role) error {
	return updateNode(func(node storm.Node) error {
		var g Guild
		if err := node.One("GuildID", guildID, &g); err != nil {
			return err
		}

		for i, id := range g.Wheelies {
			if id == r.ID {
				g.Wheelies = deleteString(g.Wheelies, i)
				goto Save
			}
		}

		g.Wheelies = append(g.Wheelies, r.ID)

	Save:
		return node.Save(&g)
	})
}

// GetRoles returns a list of Discord roles.
func GetRoles(d *discordgo.Session, guildID string) (admin, wheelies []*discordgo.Role, err error) {
	var g Guild
	if err := node.One("GuildID", guildID, &g); err != nil {
		return nil, nil, err
	}

	rs, err := d.GuildRoles(guildID)
	if err != nil {
		return nil, nil, err
	}

	admin = make([]*discordgo.Role, 0, len(g.AdminRoles))
	wheelies = make([]*discordgo.Role, 0, len(g.Wheelies))

	// I didn't skip whatever's in the loop after it's
	// found, as there may be duplicates.
	for _, r := range rs {
		for _, id := range g.AdminRoles {
			if id == r.ID {
				admin = append(admin, r)
			}
		}

		for _, id := range g.Wheelies {
			if id == r.ID {
				wheelies = append(wheelies, r)
			}
		}
	}

	return admin, wheelies, nil
}

// UserisAdmin checks if the user is an admin in the guild
func UserisAdmin(d *discordgo.Session, guildID, userID string) (bool, error) {
	var g Guild
	if err := node.One("GuildID", guildID, &g); err != nil {
		return false, err
	}

	m, err := d.State.Member(guildID, userID)
	if err != nil {
		m, err = d.GuildMember(guildID, userID)
		if err != nil {
			return false, err
		}
	}

	for _, r := range m.Roles {
		for _, id := range g.AdminRoles {
			if id == r {
				return true, nil
			}
		}
	}

	return false, nil
}

// UserisWheel checks if the user is a wheelie in the guild
func UserisWheel(d *discordgo.Session, guildID, userID string) (bool, error) {
	var g Guild
	if err := node.One("GuildID", guildID, &g); err != nil {
		return false, err
	}

	m, err := d.State.Member(guildID, userID)
	if err != nil {
		m, err = d.GuildMember(guildID, userID)
		if err != nil {
			return false, err
		}
	}

	for _, r := range m.Roles {
		for _, id := range g.AdminRoles {
			if id == r {
				return true, nil
			}
		}

		for _, id := range g.Wheelies {
			if id == r {
				return true, nil
			}
		}
	}

	return false, nil
}

func updateNode(f func(node storm.Node) error) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}

func deleteString(a []string, i int) []string {
	a[i] = a[len(a)-1]
	return a[:len(a)-1]
}

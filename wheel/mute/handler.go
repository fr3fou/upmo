package mute

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

// GuildMemberAdd is the handler to re-mute someone if they rejoin
func GuildMemberAdd(d *discordgo.Session, m *discordgo.GuildMemberAdd) {
	var mute Mute
	if err := db.From(moduleName).One("GuildID", m.GuildID, &mute); err != nil {
		logger.Error(err)
		return
	}

	for _, r := range mute.MutedUsers {
		if r == m.User.ID {
			err := d.GuildMemberRoleAdd(m.GuildID, m.User.ID, mute.MuteRole)
			if err != nil {
				logger.Error(err)
			}

			return
		}
	}
}

package wheel

import (
	"errors"
	"sync"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/wheel"
)

var (
	errUnknownVote       = errors.New("Unknown vote type")
	errVoteAlreadyExists = errors.New("Vote already exists")

	// ErrPermissionDenied is returned when the command invoker isn't in the Admin role
	ErrPermissionDenied = errors.New("Permission denied")

	// ErrClosed is returned when a vote is finished.
	ErrClosed = errors.New("Vote is closed")
)

// Vote is the structure for a vote
type Vote struct {
	guild *Guild
	d     *discordgo.Session

	GuildID string `storm:"unique,id"`

	// Initiator is the person who initiated the vote.
	// This key is kept unique to prevent multiple votes from
	// one person.
	Initiator string `storm:"unique"`

	// TargetUser is the person who was targeted
	TargetUser string `storm:"unique"`

	sync.Mutex
	Votes map[string]bool

	// VoteType should be one of the `actions`' interfaces.
	// When the threshold is passed, the VoteType is initiated.
	VoteType Action

	// StatusMessageID is the message logged for the vote
	// Todo: make this a separate module and abstract things
	// like when the message was deleted
	StatusMessageID string `storm:"unique"`
}

// NewVote starts a new vote
func NewVote(d *discordgo.Session, guildID, initiator, target string, vt Action) (*Vote, error) {
	g, err := d.State.Guild(guildID)
	if err != nil {
		return nil, err
	}

	gdb, err := node.From("votes").Begin(true)
	if err != nil {
		return nil, err
	}

	defer gdb.Rollback()

	var gs Guild
	if err := node.One("GuildID", guildID, &gs); err != nil {
		return nil, err
	}

	v := &Vote{
		guild:      &gs,
		d:          d,
		GuildID:    g.ID,
		Initiator:  initiator,
		TargetUser: target,
		VoteType:   vt,
	}

	if err := gdb.Save(&v); err != nil {
		if err == storm.ErrAlreadyExists {
			return nil, errVoteAlreadyExists
		}

		return nil, err
	}

	if err := gdb.Commit(); err != nil {
		return nil, err
	}

	return v, nil
}

// Vote casts a vote
func (v *Vote) Vote(ctx *exrouter.Context, member string, choice bool) error {
	b, err := wheel.UserisWheel(ctx.Ses, v.guild.GuildID, member)
	if err != nil {
		return err
	}

	if !b {
		return wheel.ErrPermissionDenied
	}

	v.Lock()
	defer v.Unlock()

	v.Votes[member] = choice

	var score int

	for _, b := range v.Votes {
		if b {
			score++
		} else {
			score--
		}
	}

	switch {
	case score >= v.guild.Threshold:
		m, err := v.Act()
		if err != nil {
			return err
		}

		ctx.Reply(m)
		return ErrClosed

	case score <= v.guild.Threshold*-1:
		m, err := v.Veto()
		if err != nil {
			return err
		}

		ctx.Reply(m)
		return ErrClosed

	default:
		return nil
	}
}

// Act runs the action then clean up.
func (v *Vote) Act() (string, error) {
	defer v.cleanup()

	return v.VoteType.Act(
		db.From(v.VoteType.Name()),
		v.d, v,
	)
}

// Veto cleans everything up.
func (v *Vote) Veto() (string, error) {
	return "Vote has been vetoed.", v.cleanup()
}

func (v *Vote) cleanup() error {
	node, err := node.From("votes").Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := node.DeleteStruct(v); err != nil {
		return err
	}

	return node.Commit()
}
